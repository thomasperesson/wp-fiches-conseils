<div class="entry-content">
    <?php if (get_field('fcImage')) : ?>
        <figure class="entry-image">
            <img src="<?php echo get_field('fcImage'); ?>" alt="<?php echo get_the_title(); ?>">
        </figure>
    <?php endif; ?>

    <?php if (get_field('fcDescription')) : ?>
        <div class="entry-description">
            <h3 class="infos__title">Descriptions</h3>
            <div class="entry-description__content">
                <?php echo get_field('fcDescription'); ?>
            </div>
        </div>
    <?php endif; ?>
    
    <div class="entry-infos">
        <?php if (get_field('fcPourQui')) : ?>
            <div class="content-pour-qui" data-trigger="accordion">
                <h3 class="infos__title">Pour qui ?</h3>
                <div class="content-pour-qui__content" data-accordion>
                    <?php echo get_field('fcPourQui'); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (get_field('fcPourquoi')) : ?>
            <div class="content-pourquoi" data-trigger="accordion">
                <h3 class="infos__title">Pourquoi ?</h3>
                <div class="content-pourquoi__content" data-accordion>
                    <?php echo get_field('fcPourquoi'); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (get_field('fcComment')) : ?>
            <div class="content-comment" data-trigger="accordion">
                <h3 class="infos__title">Comment ?</h3>
                <div class="content-comment__content" data-accordion>
                    <?php echo get_field('fcComment'); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (get_field('fcMiseEnGarde')) : ?>
            <div class="content-mise-en-garde" data-trigger="accordion">
                <h3 class="infos__title">Mise en garde</h3>
                <div class="content-mise-en-garde__content" data-accordion>
                    <?php echo get_field('fcMiseEnGarde'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div><!-- .entry-content -->