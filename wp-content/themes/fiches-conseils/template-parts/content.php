<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fiches_Conseils
 */
?>

<?php
$is_column = "";
if (!is_singular()) {
	$is_column = "column";
}
?>
<article id="fiche-conseils-<?php the_ID(); ?>" <?php post_class($is_column ." is-4 " . get_categories_slug_str(null, " ")); ?>>
	<header class="entry-header">
		<?php
		if (is_singular()) :
			the_title('<h1 class="entry-title">', '</h1>');
		else :
			the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
		endif;

		if ('fiche-conseils' === get_post_type()) :
		?>
			<div class="entry-meta">
				<?php
				fiches_conseils_posted_on();
				//fiches_conseils_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<section>
		<?php
		if (is_singular()) :
			get_template_part('template-parts/content-single');
		elseif (is_search()) :
			get_template_part('template-parts/content-search');
		else :
			get_template_part('template-parts/content-list');
		endif;
		?>
	</section><!-- .entry-content -->

	<footer class="entry-footer">
		<?php fiches_conseils_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->