<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fiches_Conseils
 */

?>

<article id="fiche-conseils-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section>
		<?php if (get_field('fcImage')) : ?>
			<figure class="entry-image">
				<img src="<?php echo get_field('fcImage'); ?>" alt="<?php echo get_the_title(); ?>">
			</figure>
		<?php endif; ?>
		<?php the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>'); ?>
	</section>
	<footer class="entry-footer">
		<?php fiches_conseils_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->