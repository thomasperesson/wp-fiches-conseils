<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fiches_Conseils
 */
?>
<div class="entry-content">
	<?php if (get_field('fcImage')) : ?>
		<figure class="entry-image">
			<img class="img-fluid" src="<?php echo get_field('fcImage'); ?>" alt="<?php echo get_the_title(); ?>">
		</figure>
	<?php endif; ?>
</div>