const containerFiches = document.querySelector(".container--fiches");

const isotope = new Isotope(containerFiches, {
    itemSelector: ".fiche-conseils",
    layoutMode: "fitRows",
});
