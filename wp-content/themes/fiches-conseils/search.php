<?php

/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Fiches_Conseils
 */

get_header();
?>

<main id="primary" class="site-main">
	<div class="container container--fiches">
		<?php if (have_posts()) : ?>
			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf(esc_html__('Résultat de la recherche pour : %s', 'fiches-conseils'), '<span>' . get_search_query() . '</span>');
					?>
				</h1>
			</header><!-- .page-header -->
		<?php
			/* Start the Loop */
			while (have_posts()) : the_post();
				get_template_part('template-parts/content-search');
			endwhile;
			the_posts_navigation();
		else :
			get_template_part('template-parts/content', 'none');
		endif;
		?>
	</div>
</main><!-- #main -->

<?php
get_sidebar();
get_footer();
