<?php
$args = [
    'posts_per_page' => -1,
    'post_type' => 'fiche-conseils',
    'orderby' => 'title',
    // 'orderby' => 'publish_date',
    'order' => 'ASC',
];

$wp_query = new WP_Query($args);
?>

<?php get_header(); ?>
<?php get_sidebar(); ?>
<main id="primary" class="container site-main">
    <!-- <div class="colmuns button-group filter-button-group">
        <button class="column" data-filter="*">show all</button>
        <button class="column" data-filter=".categorie-1">Catégorie 1</button>
        <button class="column" data-filter=".categorie-2">Catégorie 2</button>
        <button class="column" data-filter=".digestion">Digestion</button>
    </div> -->
    <div class="columns is-flex-wrap-wrap container--fiches">
        <?php while ($wp_query->have_posts()) : the_post(); ?>
            <?php
                // if (is_front_page()) {
                //     get_template_part('template-parts/content', get_post_type());
                // } elseif(is_singular())
            ?>
            <?php get_template_part('template-parts/content', get_post_type()); ?>
        <?php endwhile; ?>
    </div>
</main>



<?php
get_footer();
?>